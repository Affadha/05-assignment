#include<stdio.h>

#define PerformanceCost 500
#define PerAttendee 3


   int	NumOfAttendees(int price)
   {
	return 180-4*price;
}

int Income(int price){
	return NumOfAttendees(price)*price;
}

int Cost(int price){
	return NumOfAttendees(price)*3+500;
}

int Profit(int price){
	return Income(price)-Cost(price);
}

int main(){
	int price;
	printf("\nExpected Profit for Ticket Prices: \n\n");
	for(price=5;price<=45;price+=5)
	{
		printf("If Ticket Price = Rs.%d \t\t Then Profit = Rs.%d\n",price,Profit(price));
		printf("-----------------------------------------------------\n\n");
    }
		return 0;
	}


